const {WebhookClient} = require('dialogflow-fulfillment')
const express = require('express')()

// express.set('view engine', 'ejs')
express.get('/',(req,res)=>{
//    res.render('pages/index',{test : "salut"})

    const agent = new WebhookClient({request: req,response: res})

    function fallback(agent){
        agent.add("désolé je n'ai pas entendu veuillez repeter svp")
    }

    let intentMap = new Map()
    intentMap.set('test',fallback)

    agent.handleRequest(intentMap)
})

express.listen(8080)